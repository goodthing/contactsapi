﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactsAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace ContactsAPI.Controllers
{
    [Route("api/[controller]")]
    public class ContactsController : Controller
    {
        private readonly ContactsContext _context;

        public ContactsController(ContactsContext context)
        {
            _context = context;

            if (_context.Contacts.Count() == 0)
            {
                _context.Contacts.Add(new Contact { FirstName = "Tony", LastName="Stark" ,Email="mail@example.com"});
                _context.Contacts.Add(new Contact { FirstName = "Peter", LastName = "Parker", Email = "mail@example.com" });
                _context.Contacts.Add(new Contact { FirstName = "Clark", LastName = "Kent", Email = "mail@example.com" });
                _context.Contacts.Add(new Contact { FirstName = "Helena", LastName = "Bertinelli", Email = "mail@example.com" });
                _context.Contacts.Add(new Contact { FirstName = "Bruce", LastName = "Wayne", Email = "mail@example.com" });
                _context.Contacts.Add(new Contact { FirstName = "Barry", LastName = "Allen", Email = "mail@example.com" });
                _context.Contacts.Add(new Contact { FirstName = "James", LastName = "Howlett", Email = "mail@example.com" });
                _context.Contacts.Add(new Contact { FirstName = "Anna", LastName = "Marie", Email = "mail@example.com" });
                _context.Contacts.Add(new Contact { FirstName = "James Buchanan", LastName = "Barnes", Email = "mail@example.com" });
                _context.Contacts.Add(new Contact { FirstName = "Jessica", LastName = "Drew", Email = "mail@example.com" });
                _context.SaveChanges();
            }
        }
        
        [HttpGet]
        public List<Contact> Get()
        {
            return _context.Contacts.ToList();
        }

        [HttpGet("{id}", Name = "GetContact")]
        public IActionResult GetById(int id)
        {
            var contact = _context.Contacts.Find(id);
            if (contact == null)
            {
                return NotFound();
            }
            return Ok(contact);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Contact contact)
        {
            if (contact == null)
            {
                return BadRequest();
            }

            _context.Contacts.Add(contact);
            _context.SaveChanges();

            return CreatedAtRoute("GetContact", new { id = contact.Id }, contact);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Contact update)
        {
            if (update == null || update.Id != id)
            {
                return BadRequest();
            }

            var contact = _context.Contacts.Find(id);
            if (contact == null)
            {
                return NotFound();
            }

            contact.FirstName = update.FirstName;
            contact.LastName = update.LastName;
            contact.Telephone = update.Telephone;
            contact.Email = update.Email;

            _context.Contacts.Update(contact);
            _context.SaveChanges();

            return CreatedAtRoute("GetContact", contact);
        }
    }
}
